export default {
  "keyboard typing speed test": "Keyboard typing speed test",
  restart: "Restart",
  "Please be prepared": "Please be prepared",
  "start typing now": "Start typing now",
  "time": "time",
  "error count": "error count",
};
