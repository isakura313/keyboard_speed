export default {
  "keyboard typing speed test": "Тест скорости печати",
  restart: "Перезапуск",
  "Please be prepared": "Пожалуйста, подготовьтесь",
  "start typing now": "Start typing now",
  time: "Время",
  "error count": "Количество ошибок",
};
