import { createRouter, createWebHistory } from "vue-router";
import App from "../App.vue";
import Test from "../pages/Test.vue";
import Home from "../pages/Home.vue";
import Learning from "../pages/Learning.vue";
const routes = [
    { 
        path: "/", 
        component: Home 
    },
    { 
        path: "/Test", 
        component: Test 
    },
    { 
        path: "/Learning", 
        component: Learning 
    },

    ];

const router = createRouter({
  // 4. Provide the history implementation to use. We are using the hash history for simplicity here.
  history: createWebHistory(),
  routes, // short for `routes: routes`
});
export default router;
