import { createApp } from "vue";
import { createI18n } from "vue-i18n";
import messages from "./messages";
// import router from './router'
import "./style.css";
import App from "./App.vue";

const i18n = createI18n({
  locale: "en",
  messages,
});

createApp(App)
  // .use(router)
  .use(i18n)
  .mount("#app");
