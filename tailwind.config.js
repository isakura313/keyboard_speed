// tailwind.config.js
module.exports = {
  purge: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
  darkMode: false, // or 'media' or 'class'
  theme: {
    colors: {
      green: "#18a058",
      grey: "#f4f4f5",
      "grey-300": "#cbd5e1",
      blue: "#2080f0",
      white: "#ffffff",
    },
    extend: {},
  },
  variants: {
    extend: {},
  },
  plugins: [],
};
